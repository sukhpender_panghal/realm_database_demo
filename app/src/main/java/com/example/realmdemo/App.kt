package com.example.realmdemo

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration

class App :Application(){
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)

        val config2 = RealmConfiguration.Builder()
            .name("default2")
            .schemaVersion(1)
            .deleteRealmIfMigrationNeeded()
             .build()
        Realm.setDefaultConfiguration(config2)

    }

}