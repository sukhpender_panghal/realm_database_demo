package com.example.realmdemo

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), StudentInterface {

    private lateinit var realm1: Realm
    private lateinit var realm: Realm
    val stud = Student()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //realm = Realm.getInstance(config2)
        //realm1 = Realm.getDefaultInstance()
       realm1 = Realm.getDefaultInstance()
        /* realm1.beginTransaction()
        realm1.deleteAll()
        realm1.commitTransaction()*/

        onClicks()
    }

    private fun onClicks() {
        btn_save.setOnClickListener {
            saveData()
        }
        btn_delete.setOnClickListener {
            delStudent(realm1, txt_id.text.toString().toInt())
        }

        btn_update.setOnClickListener {
             val findFirst = Realm.getDefaultInstance().where(Student::class.java).findFirst()
            findFirst?.let {
                Realm.getDefaultInstance().executeTransaction {
                    findFirst.name = edt_name.text.toString()
                    findFirst.age = edt_age.text.toString().toInt()
                  //  editStudent( findFirst)
                }
                readData(Realm.getDefaultInstance())
            }



        }
    }

    private fun saveData() {
        realm1.executeTransactionAsync({
            val student = Student()
            student.ID=txt_id.text.toString().toInt()
            student.name = edt_name.text.toString()
            student.age = edt_age.text.toString().toInt()
            it.copyToRealmOrUpdate(student)
        }, {
            Log.d("asd", "Success")
            readData(realm1)
        }, {
            Log.d("asd", "Error")
        })
    }

    private fun readData(realm: Realm) {
        val student = realm.where(Student::class.java).findAll()
        var response = ""
        student.forEach {
            response = response + "Id: ${it.ID}, Name: ${it.name}, Age: ${it.age}" + "\n"
        }
        db_response.text = response
        //realm.where(Student::class.java).sort("ID")
    }

    override fun addStudent(realm: Realm, student: Student): Boolean {
        realm.beginTransaction()
        realm.copyToRealmOrUpdate(student)
        realm.commitTransaction()
        return true
    }

    override fun delStudent(realm: Realm, _ID: Int): Boolean {

        realm.beginTransaction()
        val a =
            realm.where(Student::class.java)
                .equalTo("ID", txt_id.text.toString().toInt())
                .findAll()
        a.deleteAllFromRealm()
        realm.commitTransaction()
        readData(realm)
        return true
    }

    override fun editStudent( student: Student): Boolean {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            it.copyToRealmOrUpdate(student)
        }

        readData( Realm.getDefaultInstance())
        return true
    }
}