package com.example.realmdemo

import io.realm.Realm

interface StudentInterface {
    fun addStudent(realm: Realm, student: Student): Boolean
    fun delStudent(realm: Realm, ID: Int): Boolean
    fun editStudent(student: Student): Boolean
}