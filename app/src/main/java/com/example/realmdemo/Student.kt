package com.example.realmdemo

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Student(@PrimaryKey
                   var ID: Int= 0,
                   var name: String?= null,
                   var age: Int?= null
) : RealmObject()